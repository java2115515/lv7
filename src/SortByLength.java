import java.util.Arrays;
import java.util.Scanner;

public class SortByLength {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Unesite niz riječi: ");
        String[] rijeci = scanner.nextLine().split( " ");
        Arrays.sort(rijeci, (s1, s2) -> s1.length() - s2.length());

        System.out.println("Sortirane riječi: ");
        for (String rijec : rijeci){
            System.out.println(rijec);
        }

    }
}
