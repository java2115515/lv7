import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Prosjek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> ocjene = new ArrayList<>();

        System.out.println("Unesite ocjene za studente (0 za prekid unosa): ");
        int grade = scanner.nextInt();
        while (grade != 0) {
            ocjene.add(grade);
            grade = scanner.nextInt();
        }

        System.out.println("Unesite metodu za izracun prosjeka: ");
        System.out.println("A - aritmetički prosjek");
        System.out.println("G - geometrijski prosjek");
        char choice = scanner.next().charAt(0);

        double average;
        if (choice == 'A' || choice == 'a') {
            average = ocjene.stream()
                    .mapToInt(Integer::intValue)
                    .average()
                    .orElse(0.0);
        } else if (choice == 'G' || choice == 'g') {
            average = Math.pow(ocjene.stream()
                    .mapToDouble(Double::valueOf)
                    .reduce(1, (a, b) -> a * b), 1.0 / ocjene.size());
        } else {
            System.out.println("Pogrešan unos. Odabirem aritmetički prosjek.");
            average = ocjene.stream()
                    .mapToInt(Integer::intValue)
                    .average()
                    .orElse(0.0);
        }

        if (choice == 'A' || choice == 'a') {
            System.out.printf("Aritmetički prosjek: %.2f", average);
        } else {
            System.out.printf("Geometrijski prosjek: %.2f", average);
        }
    }
}



