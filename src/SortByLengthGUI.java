import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Comparator;

public class SortByLengthGUI {
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private JTextField inputField;
    private JTextArea outputArea;
    private JButton sortButton;

    public SortByLengthGUI() {
        // Create the frame and panel
        frame = new JFrame("Sort By Length");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Create the input field and label
        label = new JLabel("Unesite niz riječi:");
        inputField = new JTextField(20);

        // Create the output area
        outputArea = new JTextArea(10, 20);
        outputArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(outputArea);

        // Create the sort button and action listener
        sortButton = new JButton("Sortiraj");
        sortButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String input = inputField.getText();
                String[] rijeci = input.split(" ");
                Arrays.sort(rijeci, (s1, s2) -> s1.length() - s2.length());
                outputArea.setText("Sortirane riječi:\n");
                for (String rijec : rijeci) {
                    outputArea.append(rijec + "\n");
                }
            }
        });

        // Add the components to the panel
        panel.add(label);
        panel.add(inputField);
        panel.add(sortButton);
        panel.add(scrollPane);

        // Add the panel to the frame and set the size
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SortByLengthGUI gui = new SortByLengthGUI();
    }
}
