import javax.swing.*;

public class GUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("My GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Create a new JLabel (text label)
        JLabel label = new JLabel("Hello, world!");

        // Add the label to the frame
        frame.getContentPane().add(label);

        // Set the size and make the frame visible
        frame.pack();
        frame.setVisible(true);
    }
}
